﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBook
{

    public class Program
    {
        public static void Main()
        {
            NoteBook contactList = new NoteBook();
            var continueFlag = true;
            while (continueFlag)
            {
                Console.Clear();
                Console.WriteLine("Hi, Dear! This is your NoteBook! \nPlease, choose one of the following options:");
                Console.Write("1.Create note;\n2.Edit note;\n3.Delete note;\n4.Show all notes (full list);" +
                    "\n5.Show all notes(short list);\n6.Exit\n");

                if (!int.TryParse(Console.ReadLine(), out var n))
                {
                    continue;
                }
                switch (n)
                {
                    case 1:
                        {
                            contactList.AddNote();
                            break;
                        }
                    case 2:
                        {
                            contactList.EditNote();
                            Console.ReadKey();
                            break;
                        }
                    case 3:
                        {
                            contactList.DeleteNote();
                            break;
                        }
                    case 4:
                        {
                            contactList.ShowAllNotes();
                            Console.ReadKey();
                            break;
                        }
                    case 5:
                        {
                            contactList.OutNote();
                            Console.ReadKey();
                            break;
                        }
                    case 6:
                        System.Environment.Exit(1);
                        continueFlag = false;
                        break;

                    default:
                        {
                            Console.WriteLine("Error! Enter the number from 1 to 6");
                            break;
                        }
                }
            }
            Console.ReadKey();
        }
    }
}