﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace NoteBook
{
    class NoteBook
    {

        private Dictionary<int, Note> notePerson { get; set; } = new Dictionary<int, Note>();

        /*-------------Create note------------*/
        public void AddNote()
        {
            bool continueFlag = true;
            string pattern = @"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$";
            while (continueFlag)
            {

                    string surname;
                    do
                    {
                        Console.Write("Enter the Surname: ");
                        surname = Console.ReadLine();
                    }
                    while (string.IsNullOrWhiteSpace(surname));
                    string name;
                    do
                    {
                        Console.Write("Enter the Name: ");
                        name = Console.ReadLine();
                    }
                    while (string.IsNullOrWhiteSpace(name));
                    Console.Write("Enter the Middle Name: ");
                    string secname = Console.ReadLine();
                    string numPhone;
                    do
                    {
                    Console.Write("Phone number: ");
                    numPhone = Console.ReadLine();
                    }                   
                    while (!Regex.IsMatch(numPhone, pattern));
                    string country;
                    do
                    {
                        Console.Write("Country: ");
                        country = Console.ReadLine();
                    }
                    while (string.IsNullOrWhiteSpace(country));
                    Console.Write("BirthDay: ");
                    string birthDate = Console.ReadLine();
                    Console.Write("Enter the organisation's name: ");
                    string organization = Console.ReadLine();
                    Console.Write("Enter the position: ");
                    string position = Console.ReadLine();
                    Console.Write("Any other information: ");
                    string comments = Console.ReadLine();

                    notePerson.Add(Note.GetNextId(), new Note(surname, name, secname, numPhone, country, birthDate, organization, position, comments));
                    continueFlag = false;
            
            }
            Console.WriteLine();
        }

        /*-----Show all notes in the NoteBook-----*/
        public void ShowAllNotes()
        {
            if (notePerson.Count > 0)
            {
                foreach (var people in notePerson)
                {
                    Console.WriteLine("Id: {0}; Surname: {1}; Name: {2}; Midname: {3}; Number: {4}; Country: {5}; BirthDay: {6}; Organisations: {7}; Position: {8}; Others: {9}",
                        people.Key, people.Value.Surname, people.Value.Name, people.Value.Midname, people.Value.NumPhone, people.Value.Country, people.Value.BirthDate, people.Value.Organization, people.Value.Position, people.Value.Others);
                }
            }
            else
            {
                Console.WriteLine("No notes.");
            }
        }

        /*-----Show all notes ( short card )-----*/
        public void OutNote()
        {
            if (notePerson.Count > 0)
            {
                foreach (var people in notePerson)
                {
                    Console.WriteLine("Id: {0}; Surname: {1}; Name: {2}; Number: {3}", people.Key, people.Value.Surname, people.Value.Name, people.Value.NumPhone);
                }
            }
            else
            {
                Console.WriteLine("No notes.");
            }
        }
        
        /*------------------Edit note-----------------*/
        public void EditNote()
        {
            Console.Write("Enter the contact's ID: ");
            if (!int.TryParse(Console.ReadLine(), out var id))
            {
                Console.WriteLine("Contact's ID has to be a number!");
            }
            else if (notePerson.ContainsKey(id))
            {

                Console.WriteLine("1.Surname: {0};\n2.Name: {1};\n3.Midname: {2};\n4.Number: {3};\n5.Country: {4};\n6.BitrhDay: {5};\n7.Organisation: {6};\n8.Position: {7};\n9.Others: {8}\n10.Exit",
                    notePerson.ElementAt(id).Value.Surname, notePerson.ElementAt(id).Value.Name, notePerson.ElementAt(id).Value.Midname,
                    notePerson.ElementAt(id).Value.NumPhone, notePerson.ElementAt(id).Value.Country, notePerson.ElementAt(id).Value.BirthDate,
                    notePerson.ElementAt(id).Value.Organization, notePerson.ElementAt(id).Value.Position, notePerson.ElementAt(id).Value.Others);


                var continueFlag = true;
                while (continueFlag)
                {
                    Console.Write("Enter the number of parametr that you wanna change: ");
                    if (!int.TryParse(Console.ReadLine(), out var nameField))
                    {
                        continue;
                    }
                    switch (nameField)
                    {
                        case 1:
                            {
                                Console.Write("Enter new value for Surname: ");
                                notePerson.ElementAt(id).Value.Surname = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 2:
                            {
                                Console.Write("Enter new value for Name: ");
                                notePerson.ElementAt(id).Value.Name = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 3:
                            {
                                Console.Write("Enter new value for Middle name: ");
                                notePerson.ElementAt(id).Value.Midname = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 4:
                            {
                                Console.Write("Enter new value for number: ");
                                notePerson.ElementAt(id).Value.NumPhone = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 5:
                            {
                                Console.Write("Enter new value for country: ");
                                notePerson.ElementAt(id).Value.Country = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 6:
                            {
                                Console.Write("Enter new value for bithDay: ");
                                notePerson.ElementAt(id).Value.BirthDate = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 7:
                            {
                                Console.Write("Enter new value for organisation:  ");
                                notePerson.ElementAt(id).Value.Organization = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 8:
                            {
                                Console.Write("Enter new value for position:  ");
                                notePerson.ElementAt(id).Value.Position = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 9:
                            {
                                Console.Write("Enter new value for others:  ");
                                notePerson.ElementAt(id).Value.Others = Console.ReadLine();
                                Console.Write("Value was changed. Do you want to change another value? (Y/N)");
                                string res = Console.ReadLine();
                                if (res == "N" || res == "n")
                                {
                                    continueFlag = false;
                                }
                                break;
                            }
                        case 10:
                            {
                                continueFlag = false;
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Error! Enter the number from 1 to 6.");
                                break;
                            }
                    }
                }
            }
            else
            {
                Console.WriteLine("No note.");
            }
        }

        /*-----------Delete note------------*/
        public void DeleteNote()
        {
            Console.Write("Enter the notes's ID: ");
            if (int.TryParse(Console.ReadLine(), out var idCont))
            {
                notePerson.Remove(idCont);
            }
        }
    }
}
