﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBook
{
     class Note
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Midname { get; set; }
        public string NumPhone { get; set; }
        public string Country { get; set; }
        public string BirthDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string Others { get; set; }
        public Note(string surname, string name, string midname, string numPhone, string country, string birthDate, string organization, string position, string others)
        {
            this.Surname = surname;
            this.Name = name;
            this.Midname = midname;
            this.NumPhone = numPhone;
            this.Country = country;
            this.BirthDate = birthDate;
            this.Organization = organization;
            this.Position = position;
            this.Others = others;
        }

        private static int SequenceId { get; set; } = 0;

        public static int GetNextId()
        {
            return SequenceId++;
        }
    }
}

